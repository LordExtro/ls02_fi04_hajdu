package kDR;
/** Package
 * 
 * @author Nicolas Hajdu
 * @author FI-C-04
 */
public class Ladung {
	private String bezeichnung;
	private int menge;
	
	public Ladung() {
		this.bezeichnung = "unbenannt";
		this.menge = 0;
	}
	public Ladung(String bezeichnung, int menge) {
		setBezeichnung(bezeichnung);
		setMenge(menge);
	}
	
	public String getBezeichnung(){
		return this.bezeichnung;
	}
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}
	public int getMenge() {
		return this.menge;
	}
	public void setMenge(int menge) {
		this.menge = menge;
	}
}
