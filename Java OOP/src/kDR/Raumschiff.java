package kDR;
/**
 * @author Nicolas Hajdu
 * @author FI-C-04
 */
import java.util.ArrayList;
import kDR.Ladung;


public class Raumschiff {
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsName;
	private ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	private ArrayList<Ladung> ladungsVerzeichnis = new ArrayList<Ladung>();
	
	public Raumschiff() {
		
		this.photonentorpedoAnzahl = 0;
		this.energieversorgungInProzent = 0;
		this.schildeInProzent = 0;
		this.huelleInProzent = 0;
		this.lebenserhaltungssystemeInProzent = 0;
		this.androidenAnzahl = 0;
		this.schiffsName = "Bruh" ;
	}
	
	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int zustandschildeInProzent,
			int zustandhuelleInProzent, int zustandlebenserhaltungssystemeInProzent, int androidenAnzahl, String schiffsName) {
		super();
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = zustandschildeInProzent;
		this.huelleInProzent = zustandhuelleInProzent;
		this.lebenserhaltungssystemeInProzent = zustandlebenserhaltungssystemeInProzent;
		this.androidenAnzahl = androidenAnzahl;
		this.schiffsName = schiffsName;
	}

	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}

	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		
	}

	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}

	public void setEnergieversorgungInProzent(int zustandenergieversorgungInProzentNeu) {
		this.energieversorgungInProzent = zustandenergieversorgungInProzentNeu;
	}

	public int getSchildeInProzent() {
		return schildeInProzent;
	}

	public void setSchildeInProzent(int zustandschildeInProzentNeu) {
		this.schildeInProzent = zustandschildeInProzentNeu;
	}

	public int getHuelleInProzent() {
		return huelleInProzent;
	}

	public void setHuelleInProzent(int zustandhuelleInProzentNeu) {
		this.huelleInProzent = zustandhuelleInProzentNeu;
	}

	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}

	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzentNeu) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzentNeu;
	}

	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}

	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}

	public String getSchiffsName() {
		return schiffsName;
	}

	public void setSchiffsName(String schiffsName) {
		this.schiffsName = schiffsName;
	}
	public ArrayList<String> getBroadcastKommunikator() {
		return broadcastKommunikator;
	}

	public void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
		this.broadcastKommunikator = broadcastKommunikator;
	}

	public ArrayList<Ladung> getLadungsVerzeichnis() {
		return ladungsVerzeichnis;
	}

	public void setLadungsVerzeichnis(ArrayList<Ladung> ladungsVerzeichnis) {
		this.ladungsVerzeichnis = ladungsVerzeichnis;
	}
	/** Vorraussetzung.: Wenn Ladung hinzugef�gt werden soll
	 * (Effekt.:) Wird mit dem aufrufen der Methode neue Ladung dem Ladungsverzeichnis hinzugef�gt.
	 * @param addLadung (Ladung neueLadung)
	 * @param this ladungsVerzeichnis add (neueLadung) f�gt der ArrayList Ladung eine benannte Ladung hinzu.
	 */
	public void addLadung(Ladung neueLadung) {
		this.ladungsVerzeichnis.add(neueLadung);
	}
	/** Vorraussetzung.: Wenn Photonentorpedos abgeschossen werden
	 * (Effekt.:) wird gepr�ft ob die Anzahl der Photonentorpedos kleiner bzw gleich 0 ist. Sollte dies der Fall sein wird die nachrichtAnAlle click ausgegeben.
	 * Ansonsten wird die Photonentorpedo Anzahl um 1 verringert die nachrichtAnAlle ausgegeben das die Topedosabgeschossen wurden und die methode treffer wird aufgerufen.
	 * @param nachrichtAnAlle ruft die Methode auf
	 * @param setPhotonentopfedoAnzahl (this photoentorpedoAnzahl - 1) verringert Photonentorpedoanzahl um 1
	 * @param treffer (r) ruft die Methode treffer f�r das getroffene Raumschiff auf
	 */
	public void photonentorpedoSchiessen(Raumschiff r) {
        if(photonentorpedoAnzahl <= 0)
        {
            nachrichtAnAlle("-=Click=-");
        }
        else
        {
            setPhotonentorpedoAnzahl(this.photonentorpedoAnzahl - 1);
            nachrichtAnAlle("Photonentorpedos abgeschossen");
            treffer(r);
        }

    }
	/** Vorraussetzung.: Wenn Phaserkanonen abgeschossen werden
	 *(Effekt.:) Wird gepr�ft ob die Energieversorgung kleiner als 50 ist und die nachricht Click ausgegeben. Ansonsten wird die Energieversorgung um 50 verringert die Methode 
	 *treffer aufgerufen und die Nachricht Phaserkanone abgeschossen ausgegeben.
	 * 
	 * @param r
	 */
	public void phaserkanoneschiessen(Raumschiff r) {
		if (getEnergieversorgungInProzent() < 50) {
			nachrichtAnAlle("-=*Click*=-");
		}
		else {
			setEnergieversorgungInProzent(energieversorgungInProzent - 50);
			treffer(r);
			nachrichtAnAlle("Phaserkanone abgeschossen!\n");
		}
	}
	/** Voraussetzung.: Wenn Treffer abgerufen wird
	 * (Effekt.:) wird ausgegeben dass das Schiff getroffen wurde und die Schilde werden um 50 verringert.
	 * Sollten die Schilde 0 betragen, werden die Huelle und die Energieversorung um 50 verringert.
	 * Sollte die Huelle 0 betragen, werden die Lebenserhaltungssysteme  auf 0 gesetzt und die Nachricht an alle ausgegeben, dass die Lebenserhaltungssysteme zerst�rt wurden.
	 * @param r getSchiffsName ist der Name des getroffenen Schiffes
	 * @param r setSchildeInProzent() setzt die Schilde des getroffenen Schiffes
	 * @param r getSchildeInProzent() ruft die Schilde des getroffenen Schiffes
	 * @param nachrichtAnAlle ruft die Methode nachrichtAnAlle auf
	 * @param r ist das Raumschiff was �bergeben wird
	 */
	private void treffer(Raumschiff r) {
		System.out.printf("\n" + r.getSchiffsName() + " wurde getroffen!\n");
		r.setSchildeInProzent(r.getSchildeInProzent() - 50);
		if  (this.schildeInProzent == 0) {
			this.huelleInProzent =- 50;
			this.energieversorgungInProzent =- 50;
		}
		if (this.huelleInProzent == 0) {
			this.lebenserhaltungssystemeInProzent = 0;
			nachrichtAnAlle("Lebenserhaltungssysteme wurde vollkommen zerst�rt.");
		}
	}
	/** Vorraussetzung.: Wenn nachrichtAnAlle aufgerufen wird
	 * (Effekt.:) gibt diese eine Nachricht von dem Raumschiff welches die Methode aufgerufen hat aus und gibt diese an den Broadcast Kommunikator weiter.
	 * @param message gibt die Nachricht als String wieder
	 * @param broadcastKommunikator add(message) f�gt die Nachricht dem broadcastKommunkator hinzu.
	 */
	public void nachrichtAnAlle(String message) {
		System.out.println("\nNachricht von Raumschiff "+ getSchiffsName()+ ": \n" + message);
		broadcastKommunikator.add(message);
	}
	/**
	 * @return broadcastKommunikator gibt die ArrayList durch den Broadcast KOmmunikator aus
	 */
	public ArrayList<String> eintraegeLogbuchZurueckgeben(){
		return broadcastKommunikator;
	
		
	}
	/** Vorraussetzung : Wenn photonentorpedosLaden aufgerufen wird
	 * (Effekt. :) wird gepr�ft ob Photonentorpedos vorhanden sind. Wenn nicht werden zwei nachrichten ausgegeben.
	 * @param anzahlTorpedos ruft die anzahl der vorhandenen Torpedos ab
	 * @param nachrichtAnAlle gibt Nachricht -=*Click*=- an Broadcast Kommunikator weiter
	 */
	public void photonentorpedosLaden(int anzahlTorpedos) {
		if(this.photonentorpedoAnzahl == 0) {
			System.out.println("Keine Photonentorpedos gefunden!");
			nachrichtAnAlle("-=*Click*=-");
		}
	}
	public void reperaturDurchfuehren(boolean schutzschilde, boolean energieversorgung, boolean schiffshuelle, int anzahlDroiden) {
		
	}
	/** Vorraussetzung. : Wenn zustandRaumschiff aufgerufen wird
	 * (Effekt. :) wird eine Liste mit den einzelnen Daten wie Zustand,Anzahl der Photonentorpedos, Schilde etc. augegeben
	 * @param getSchiffsName gibt den Schiffsnamen wieder
	 * @param getPhotonentorpedoAnzahl gibt die Anzahl der Photonentorpedos wieder
	 * @param getEnergieversorgungInProzent gibt die Energieversogung in Prozent wieder
	 * @param getSchildeInProzent gibt die Schilde in Prozent wieder
	 * @param getHuelleInProzent gibt die Huelle in Prozent wieder
	 * @param getLebenserhaltungssystemeInProzent gibt die Lebenserhaltungssysteme in Prozent wieder
	 * @param getAndroidenAnzahl gibt die Anzahl der Androiden wieder 
	 */
	public void zustandRaumschiff() {
		System.out.println("\nZustand des Raumschiffes " + getSchiffsName() + ": ");
		System.out.print("\nPhotonentorpedo Anzahl: " + getPhotonentorpedoAnzahl());
		System.out.print("\nEnergieversorgung: " + getEnergieversorgungInProzent() + "%");
		System.out.print("\nSchilde: " + getSchildeInProzent() + "%");
		System.out.print("\nHuelle: " + getHuelleInProzent() + "%");
		System.out.print("\nLebenserhaltungssysteme: " + getLebenserhaltungssystemeInProzent() + "%");
		System.out.print("\nAndroiden Anzahl: "+ getAndroidenAnzahl() + "\n");
		
	}
	/** Vorraussetzung. : Wenn ladungsverzeichnisAusgeben aufgerufen wird
	 *(Effekt. :) wird gepr�ft ob im Ladungsverzeichnis Ladung vorhanden ist. Wenn nicht wird eine Nachricht ausgegeben. Sollte Ladungs vorhanden sein, dann wird das Ladungsverzeichnis so lange gepr�ft bis alle Ladungen wiedergegeben wurden
	 *@param ladungsVerzeichnis ArrayListe mit Ladung aus der Methode Ladung
	 *@param getSchiffsName gibt den Schiffsnamen wieder
	 *@param getBezeichnung gibt den Namen der Ladung wieder
	 */
	public void ladungsverzeichnisAusgeben() {
		if(this.ladungsVerzeichnis.size() <= 0) {
			System.out.println("Es ist keine Ladung im Ladungsverzeichniss vorhanden/n");
		}
		else {
			System.out.println("\nLadungsverzeichnis von " + getSchiffsName() + ": ");
			for(int i = 0; i < this.ladungsVerzeichnis.size(); i++) {
			System.out.println(this.ladungsVerzeichnis.get(i).getBezeichnung()+": "+this.ladungsVerzeichnis.get(i).getMenge());
			}
		}
	}
	/** Vorraussetzung. : Wenn ladungsverzeichnusAufraeumen aufgerufen wird
	 * (Effekt. :) werden alle Eintraege die eine Menge von null haben aus dem Ladungsverzeichnis gel�scht
	 * @param ladungsVerzeichnis Array Liste mit Ladung aus der Methode Ladung
	 */
	public void ladungsverzeichnisAufraeumen() {
		for(int i = 0; i < this.ladungsVerzeichnis.size(); i++) {
			if(this.ladungsVerzeichnis.get(i).getMenge() == 0) {
				ladungsVerzeichnis.remove(i);
			}
		}
	}
}

	
