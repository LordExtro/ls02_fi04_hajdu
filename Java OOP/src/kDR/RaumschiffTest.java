package kDR;
/** Package
 * 
 * @author Nicolas Hajdu
 * @author FI-C-04
 * 
 */
/** Voraussetzung. : Wenn RaumschiffTest aufgerufen wird
 *(Effekt. :) werden alle Werte f�r die Klasse Raumschiff initialisiert
 * @param Raumschiff beinhaltet die Werte f�r energieversogung, schilde, Lebenserhaltungssysteme, Schiffsname etc.
 * @param Ladung beinhaltet die Werte f�r das Ladungsverzeichnis wie Bezeichnung und Menge
 * @param addLadung weist die Ladung der Klasse Ladung zu und legt fest f�r welches Raumschiff die Ladung ist
 */
public class RaumschiffTest {
	//(int photonentorpedoAnzahl, int energieversorgungInProzent, int zustandschildeInProzent,
			//int zustandhuelleInProzent, int zustandlebenserhaltungssystemeInProzent, int androidenAnzahl, String schiffsName)
	public static void main(String[] args) {
		// Raumschiffe
		
		Raumschiff klingonen = new Raumschiff(1,100,100,100,100,2,"IKS Hegh'ta");
		Raumschiff romulaner = new Raumschiff(2,100,100,100,100,2,"IRW Khazara");
		Raumschiff vulkanier = new Raumschiff(0,80,80,50,100,5,"Ni'var");
		
		// Ladung initalisieren
		Ladung ferengiSchneckensaft = new Ladung ("Ferengi Schneckensaft",200);
		Ladung klingonenSchwert = new Ladung("Bat-leth Klingonen Schwert",200);
		Ladung borgSchrott = new Ladung("Borg-Schrott",5);
		Ladung roteMaterie = new Ladung("Rote Materie",2);
		Ladung plasmaWaffe = new Ladung("Plasma-Waffe",50);
		Ladung forschungsSonde = new Ladung("Forschungssonde",35);
		Ladung photonenTorpedo = new Ladung("Photonentorpedo",3);
		
		// Ladung zuweisen
		klingonen.addLadung(ferengiSchneckensaft);
		klingonen.addLadung(klingonenSchwert);
		romulaner.addLadung(borgSchrott);
		romulaner.addLadung(roteMaterie);
		romulaner.addLadung(plasmaWaffe);
		vulkanier.addLadung(forschungsSonde);
		vulkanier.addLadung(photonenTorpedo);
		
		klingonen.photonentorpedoSchiessen(romulaner);
		romulaner.phaserkanoneschiessen(klingonen);
		vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch");
		klingonen.zustandRaumschiff();
		klingonen.ladungsverzeichnisAusgeben();
		klingonen.photonentorpedoSchiessen(romulaner);
		klingonen.photonentorpedoSchiessen(romulaner);
		klingonen.zustandRaumschiff();
		romulaner.zustandRaumschiff();
		vulkanier.zustandRaumschiff();
	}
	
}
