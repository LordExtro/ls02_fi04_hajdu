package kreis;

public class Rechteck {
	
	private double a;
	private double b;
	
	public Rechteck (double a, double b)
	{
		
		setSeiteA(a);
		setSeiteB(b);
	}
	public void setSeiteA(double a) {
		this.a = a;
	}
	public void setSeiteB(double b) {
		this.b = b;
	}
	
	public double getFlaeche() {
		return a*b;
	}
	public double getUmfang() {
		return 2*a + 2*b;
	}
	public double getDiagonale() {
		return Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
	}
}
